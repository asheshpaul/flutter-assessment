import 'package:flutter/material.dart';

Widget bottomBarButton(slideIndex,
    {index, label, onButtonPressed, buttonColor}) {
  return RaisedButton(
    onPressed: onButtonPressed,
    child: Text(label),
    padding: EdgeInsets.symmetric(vertical: 16),
    color: slideIndex == index ? Colors.grey : buttonColor ?? Colors.amber[700],
    textColor: slideIndex == index ? Colors.black : Colors.white,
  );
}
