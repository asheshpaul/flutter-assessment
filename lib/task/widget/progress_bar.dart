import 'package:flutter/material.dart';

progressBar(int slideIndex) {
  divider() => Container(
        width: 24,
        height: 2,
        color: Colors.grey[400],
      );
  statusIndicator({isActive, isComplete}) => Container(
        height: 32,
        width: 32,
        padding: EdgeInsets.all(isActive ? 2 : 4),
        decoration: BoxDecoration(
            color: Colors.black, borderRadius: BorderRadius.circular(200)),
        child: Container(
          padding: EdgeInsets.all(2),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(200)),
          child: isActive
              ? Container(
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(200)),
                  child: isComplete
                      ? Icon(Icons.done, size: 18, color: Colors.white)
                      : Center(
                          child: Text(
                          '${slideIndex + 1}',
                          style: TextStyle(color: Colors.white),
                        )),
                )
              : SizedBox(),
        ),
      );
  var index = slideIndex;
  switch (index) {
    case 0:
      {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            statusIndicator(isActive: true, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
          ],
        );
      }
      break;

    case 1:
      {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
          ],
        );
      }
      break;

    case 2:
      {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
          ],
        );
      }
      break;

    case 3:
      {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: false),
            divider(),
            statusIndicator(isActive: false, isComplete: false),
          ],
        );
      }
      break;

    case 4:
      {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: true),
            divider(),
            statusIndicator(isActive: true, isComplete: false),
          ],
        );
      }
      break;

    default:
      break;
  }
}
