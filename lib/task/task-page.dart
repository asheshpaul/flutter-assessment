import 'package:flutter/material.dart';

class TaskPage extends StatelessWidget {
  final String title;
  final int slideIndex;

  TaskPage(this.slideIndex, {this.title});

  ///stepper steps
  final List<Step> steps = [
    Step(
      title: Text(''),
      isActive: true,
      state: StepState.indexed,
      content: Container(),
    ),
    Step(
      isActive: false,
      state: StepState.editing,
      title: Text(''),
      content: Container(),
    ),
    Step(
      state: StepState.complete,
      title: Text(''),
      content: Container(),
    ),
    Step(
      state: StepState.disabled,
      title: Text(''),
      content: Container(),
    ),
    Step(
      state: StepState.error,
      title: Text(''),
      content: Container(),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ///tried with stepper
        /*Flexible(
          child: Stepper(
            steps: steps,
            currentStep: slideIndex,
            type: StepperType.horizontal,
            controlsBuilder: (BuildContext context,
                {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
              return Row(
                children: <Widget>[
                  Container(
                    child: null,
                  ),
                  Container(
                    child: null,
                  ),
                ],
              );
            },
          ),
        ),*/
        Flexible(
          flex: 4,
          child: Container(
            child: Center(
              child: Text(
                title,
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
