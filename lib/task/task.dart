import 'package:flutter/material.dart';
import 'package:flutter_assessment/task/task-page.dart';
import 'package:flutter_assessment/task/widget/bottom_bar_button.dart';
import 'package:flutter_assessment/task/widget/progress_bar.dart';

class Task extends StatefulWidget {
  @override
  _TaskState createState() => _TaskState();
}

class _TaskState extends State<Task> {
  int slideIndex = 0;
  PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: progressBar(slideIndex),
          ),
          Expanded(
              child: PageView(
            controller: pageController,
            onPageChanged: (index) => _pageChanger(index),
            children: <Widget>[
              TaskPage(slideIndex, title: 'Page 1'),
              TaskPage(slideIndex, title: 'Page 2'),
              TaskPage(slideIndex, title: 'Page 3'),
              TaskPage(slideIndex, title: 'Page 4'),
              TaskPage(slideIndex, title: 'Page 5'),
            ],
          )),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Visibility(
                  visible: slideIndex == 0 ? false : true,
                  child: Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: bottomBarButton(
                        slideIndex,
                        index: 0,
                        label: 'Previous',
                        onButtonPressed: () => slideIndex == 0
                            ? null
                            : pageController.previousPage(
                                duration: Duration(milliseconds: 300),
                                curve: Curves.linear),
                        buttonColor: Colors.blue
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: slideIndex == 0 ? 50 : 0),
                    child: bottomBarButton(
                      slideIndex,
                      index: 4,
                      label: slideIndex == 4 ? 'Done' : 'Next',
                      onButtonPressed: () => slideIndex == 4
                          ? null
                          : pageController.nextPage(
                              duration: Duration(milliseconds: 300),
                              curve: Curves.linear),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _pageChanger(index) {
    setState(() {
      slideIndex = index;
    });
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }
}
